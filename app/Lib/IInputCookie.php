<?php

namespace App\Lib;

interface IInputCookie
{

  /**
  * If $key is null, return full COOKIE array.
  * If $key is not null, return the COOKIE value for $key or NULL
  * if $clean is true, return XSS safe version of array or value
  * @param String $key
  * @return Mixed
  */
  public function cookie($key = null, $clean = false);

}