<?php

namespace App\Lib\Traits;

trait RawArray
{
	/**
	 * method to display the whole array sent by the methods defined in the Input.php file, without sanitizing it.
	 * @param  [array] $array $_POST, $_GET, $_COOKIE, $_SERVER anyone of these sent by the methods.
	 * @return [array]      
	 */
	public function rawValues($array)
	{
		//returning the array passed to it.
		return $array;
	}
}