<?php

namespace App\Lib\traits;

trait RawKeyData
{
	/**
	 * method to display the value requested as per the key specified in the index file without sanitizing it.
	 * @param  [array] $array_name name of the array from which the specfied value to be fetched.
	 * @param  [string] $key, name of the index of the array.
	 * @return [string]
	 */
	public function rawKeyValue($array_name, $key)
	{
		//returns the value of array at the key passed/
		return $array_name[$key];
	}
}