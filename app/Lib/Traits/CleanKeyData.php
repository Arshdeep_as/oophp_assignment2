<?php

namespace App\Lib\traits;

trait CleanKeyData
{
	/**
	 * method to sanitize the value requested as per the key apecified in the index file.
	 * @param  [array] $array_name name of the array from which the specfied value to be fetched.
	 * @param  [string] $key, name of the index of the array.
	 * @return [string]
	 */
	public function sanitizedKeyValue($array_name, $key)
	{
		//returning the sanitized value of particular key.
		return htmlentities(strip_tags($array_name[$key]), ENT_QUOTES, 'UTF-8', false);		
	}
}