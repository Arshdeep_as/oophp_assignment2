<?php

namespace App\Lib\Traits;

trait CleanArray
{
	/**
	 * method to sanitize the whole array sent by the methods defined in the Input.php file.
	 * @param  [array] $var $_POST, $_GET, $_COOKIE, $_SERVER anyone of these sent by the methods.
	 * @return [array]      
	 */
	public function sanitizedValues($var)
	{
		//clean array to store the sanitized values.
		$clean_array = array();
		//accessing each element of the array
		foreach ($var as $key => $value) {
			//if the element is not an array, ssanitize it and store it.
			if(!is_array($value)){
				$clean_array[$key] = htmlentities(strip_tags($value), ENT_QUOTES, 'UTF-8', false);
			}
			//else if the element is an array
			else{
				//array to store the sub array
				$new_array = array();
				foreach ($value as $row) {
				//sanitizeing each element and store it.
					array_push($new_array, htmlentities(strip_tags($row), ENT_QUOTES, 'UTF-8', false));
				}
				//add the sub array to the clean array
				$clean_array[$key] = $new_array;
			}
		}
		//return the clean array.
		return $clean_array;
		
	}
}