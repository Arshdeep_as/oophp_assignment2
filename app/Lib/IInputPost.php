<?php

namespace App\Lib;

interface IInputPost
{

  /**
  * If $key is null, return full POST array.
  * If $key is not null, return the POST value for $key or NULL
  * if $clean is true, return XSS safe version of array or value
  * @param String $key
  * @return Mixed
  */
  public function post($key = null, $clean = false);

}