<?php

namespace App\Lib;

//implements all the interfaces on our class Input.
class Input implements IInputPost, IInput, IInputCookie, IInputServer, IInputGet
{
	use \App\Lib\Traits\CleanArray;
	use \App\Lib\Traits\CleanKeyData;
	use \App\Lib\Traits\RawArray;
	use \App\Lib\Traits\RawKeyData;
  
	/**
  * If $key is null, return full POST array.
  * If $key is not null, return the POST value for $key or NULL
  * if $clean is true, return XSS safe version of array or value
  * @param String $key
  * @return Mixed        
  */
  public function post($key = null, $clean = false)
  {
    //the key is null and the clean variable is true, this means need sanitized array
  	if(is_null($key) && $clean==true){
  		//return sanitized values
  		$result = $this->sanitizedValues($_POST);
  	}
    //the key is not null and clean is false, this means a particular key raw value needs to be displayed
  	else if(!is_null($key) && $clean == false ){
  		$result = $this->rawKeyValue($_POST, $key);
  	}
    //the key is not null and clean is false, this means a particular key needs to be sanitizes
  	else if(!is_null($key) && $clean == true ){
  		$result = $this->sanitizedKeyValue($_POST, $key);
  	}
    //else raw array needs to be displayed
  	else{
  		$result = $this->rawValues($_POST);
  	}
    //returned result to be displayed on the screen
    return $result;
  }

  /**
  * If $key is null, return full COOKIE array.
  * If $key is not null, return the COOKIE value for $key or NULL
  * if $clean is true, return XSS safe version of array or value
  * @param String $key
  * @return Mixed
  */
  public function cookie($key = null, $clean = false)
  {
    //the key is null and the clean variable is true, this means need sanitized array
  	if(is_null($key) && $clean==true){
  		//return sanitized values
  		$result = $this->sanitizedValues($_COOKIE);
  	}
    //the key is not null and clean is false, this means a particular key raw value needs to be displayed
  	else if(!is_null($key) && $clean == false ){
  		$result = $this->rawKeyValue($_COOKIE, $key);
  	}
    //the key is not null and clean is false, this means a particular key needs to be sanitizes
  	else if(!is_null($key) && $clean == true ){
  		$result = $this->sanitizedKeyValue($_COOKIE, $key);
  	}
    //else raw array needs to be displayed
  	else{
  		$result = $this->rawValues($_COOKIE);
  	}
    //returned result to be displayed on the screen
    return $result;
  }

  /**
  * If $key is null, return full SERVER array.
  * If $key is not null, return the SERVER value for $key or NULL
  * if $clean is true, return XSS safe version of array or value
  * @param String $key
  * @return Mixed
  */
  public function server($key = null, $clean = false)
  {
    //the key is null and the clean variable is true, this means need sanitized array
  	if(is_null($key) && $clean==true){
  		//return sanitized values
  		$result = $this->sanitizedValues($_SERVER);
  	}
    //the key is not null and clean is false, this means a particular key raw value needs to be displayed
  	else if(!is_null($key) && $clean == false ){
  		$result = $this->rawKeyValue($_SERVER, $key);
  	}
    //the key is not null and clean is false, this means a particular key needs to be sanitizes
  	else if(!is_null($key) && $clean == true ){
  		$result = $this->sanitizedKeyValue($_SERVER, $key);
  	}
    //else raw array needs to be displayed
  	else{
  		$result = $this->rawValues($_SERVER);
  	}
    //returned result to be displayed on the screen
    return $result;
  }

  /**
  * If $key is null, return full GET array.
  * If $key is not null, return the GET value for $key or NULL
  * if $clean is true, return XSS safe version of array or value
  * @param String $key
  * @return Mixed
  */
  public function get($key = null, $clean = false)
  {
    //the key is null and the clean variable is true, this means need sanitized array
  	if(is_null($key) && $clean==true){
      //return sanitized values
      $result = $this->sanitizedValues($_GET);
    }
    //the key is not null and clean is false, this means a particular key raw value needs to be displayed
    else if(!is_null($key) && $clean == false ){
      $result = $this->rawKeyValue($_GET, $key);
    }
    //the key is not null and clean is false, this means a particular key needs to be sanitizes
    else if(!is_null($key) && $clean == true ){
      $result = $this->sanitizedKeyValue($_GET, $key);
    }
    //else raw array needs to be displayed
    else{
      $result = $this->rawValues($_GET);
    }
    //returned result to be displayed on the screen
    return $result;
  }

}